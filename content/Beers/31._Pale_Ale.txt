Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4

====== Pale Ale ======

**Type:** Mango/Apricot IPA.

Malt:
Golden Promise 6.3 kg 
Munich 1 5.1 kg
Flaked Oat 0.9 kg
Wheat 0.4 kg

Hops - continuously hopped:
25g citra
25g cascade

Hop Stand @78*C
50g citra
25g cascade

25g citra @ dry
50g cascade @ dry

