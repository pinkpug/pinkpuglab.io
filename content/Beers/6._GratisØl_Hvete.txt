Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4

====== GratisØl: Hvete ======

**Type:** Wit.
**ABV:** 6.56%.
**OG:** 1.060
**FG:** 1.010

{{/images/gratisol_hvete.png?width=512}}

The first edition of the Pink Pugs party series.
This beer is a wheat beer from 50/50 pale malt and flaked wheat, brewed to drink on New Years Eve 2016.

Hops:
Perle (3.9%)
Styrian Golding

Adjuncts:
Bitter Orange Peels
Coriander seeds
