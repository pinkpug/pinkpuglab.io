Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4

====== Disaster IPA ======

**Type:** IPA.
**ABV:** 7.6%.

An IPA where everything that could go wrong, did go wrong. Still turned out to be quite a sucessful brew.
With a FG of 1.022 the taste is somewhat sweet, with a mild bitter aftertaste. Fruity aroma with a hoppy undertone. You can really taste the hops!
