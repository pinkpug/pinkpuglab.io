Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4

====== Gose Concept ======

**Type:** Gose.
**ABV:** TBD
**OG:** 1.050 
**PH:** TBD

Malt:
6kg Pilsner
6kg Wheat

Adjuncts:
28g perle @ 45 min
32g salt @ 10 min
56g corriander seeds @ 10 min
0.5g yeast nutrients @ 10 min

Mash:
45 min @ 65*C
15 min @ 67*C
15 min @ 75*C

Instructions:

Post-rinse:
Cool to 36*C
Pre sour to 4.5pH with lactic acid after rinse (10mL?).
Pitch lacto (500 mL Biola). Sour 2-3 days to pH 3.0-3.6.

Measurements:
T0:
	pH: 5.4
	SG: 1.050
T+17.5h:
	pH: 3.84
	SG: 1.050
T+41h
	pH: 3.34
	SG: 1.050
Post-boil
	pH: 3.49
	SG: 1.050
Notes:
Forgot perle. Pitched at 10 min instead.
